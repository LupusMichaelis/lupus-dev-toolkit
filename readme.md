# Lupus' DevOps toolkit

This is a serie of tools for the developer lost in DevOps.

# Contributions

You're welcome to contribute. There is no proper guidelines or anything yet, feel free to
provide for it, and go through the
[project](/LupusMichaelis/lupus-dev-toolkit/projects/1)
to see what is to be done.

# Internals

## Ygor

`./bin/ygor` is a script providing shorthands for daily common tasks. Invoking it without
argument triggers the usage message. I usually make in an alias `y` to it.

## Helpers

A collection of shell scripts defining helper functions for usual daily development tasks.

* `csv.sh`: functions to manipulate spreadsheets from the command line
* `curl.sh`: [cUrl](https://curl.se/) session helpers (oauth token management)
* `json.sh`: manipulation of [JSON](https://www.json.org/json-en.html) payloads
* `mkdir-cd.sh`: make a directory and jump into it

## Docker images

Images are defined in various directories listed under `build`.

The `./bin/build-images` script helps to build, sign and push images to
[Docker Hub](https://hub.docker.com/u/lupusmichaelis).

There is currently 2 different image flavours:

* hacking time images:
  images designed to embed tools for development and to be easy the use with bind mount
* shipping time images:
  images designed to be lean and secured, for safe shipping of your hand-crafted marvel

### Igor

A bare base image to embed in-container shell script helpers. Used to simplify Docker
Entrypoint and general operations done through `Dockerfile`.

# Status


[![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/trunk/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/trunk)

| Image | Version | Status |
| --- | --- | --- |
| [alpine-apache](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-apache) | 1.2.1 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-apache@1.2.1/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-apache@1.2.1) |
| [alpine-dev](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-dev) | 1.2.4 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-dev@1.2.4/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-dev@1.2.4) |
| [alpine-elm](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-elm) | 1.2.1 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-elm@1.2.1/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-elm@1.2.1) |
| [alpine-ember](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-ember) | 1.2.1 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-ember@1.2.1/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-ember@1.2.1) |
| [alpine](https://hub.docker.com/repository/docker/lupusmichaelis/alpine) | 1.2.4 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine@1.2.4/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine@1.2.4) |
| [alpine-legacy-dev](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-legacy-dev) | 1.2.1 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-legacy-dev@1.2.1/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-legacy-dev@1.2.1) |
| [alpine-legacy](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-legacy) | 1.2.1 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-legacy@1.2.1/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-legacy@1.2.1) |
| [alpine-lighttpd-front](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-lighttpd-front) | 1.2.3 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-lighttpd-front@1.2.3/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-lighttpd-front@1.2.3) |
| [alpine-lighttpd](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-lighttpd) | 1.2.4 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-lighttpd@1.2.4/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-lighttpd@1.2.4) |
| [alpine-lighttpd-proxy](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-lighttpd-proxy) | 1.2.4 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-lighttpd-proxy@1.2.4/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-lighttpd-proxy@1.2.4) |
| [alpine-lighttpd-proxy-single](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-lighttpd-proxy-single) | 1.2.4 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-lighttpd-proxy-single@1.2.4/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-lighttpd-proxy-single@1.2.4) |
| [alpine-lighttpd-secure-front](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-lighttpd-secure-front) | 1.2.4 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-lighttpd-secure-front@1.2.4/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-lighttpd-secure-front@1.2.4) |
| [alpine-lighttpd-secure](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-lighttpd-secure) | 1.2.4 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-lighttpd-secure@1.2.4/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-lighttpd-secure@1.2.4) |
| [alpine-mariadb](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-mariadb) | 1.2.1 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-mariadb@1.2.1/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-mariadb@1.2.1) |
| [alpine-mariadb-secure](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-mariadb-secure) | 1.2.1 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-mariadb-secure@1.2.1/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-mariadb-secure@1.2.1) |
| [alpine-nodejs](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-nodejs) | 1.2.1 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-nodejs@1.2.1/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-nodejs@1.2.1) |
| [alpine-php7-composer](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-php7-composer) | 1.2.1 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-php7-composer@1.2.1/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-php7-composer@1.2.1) |
| [alpine-php7-fpm-composer](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-php7-fpm-composer) | 1.2.1 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-php7-fpm-composer@1.2.1/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-php7-fpm-composer@1.2.1) |
| [alpine-php7-fpm](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-php7-fpm) | 1.2.1 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-php7-fpm@1.2.1/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-php7-fpm@1.2.1) |
| [alpine-php7](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-php7) | 1.2.1 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-php7@1.2.1/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-php7@1.2.1) |
| [alpine-php8-composer](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-php8-composer) | 1.2.4 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-php8-composer@1.2.4/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-php8-composer@1.2.4) |
| [alpine-php8-fpm](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-php8-fpm) | 1.2.4 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-php8-fpm@1.2.4/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-php8-fpm@1.2.4) |
| [alpine-php8](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-php8) | 1.2.4 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-php8@1.2.4/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-php8@1.2.4) |
| [alpine-postfix](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-postfix) | 1.2.1 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-postfix@1.2.1/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-postfix@1.2.1) |
| [alpine-mutt](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-mutt) | 1.2.2 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-mutt@1.2.2/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-postfix-mutt@1.2.2) |
| [alpine-proxy](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-proxy) | 1.2.1 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-proxy@1.2.1/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-proxy@1.2.1) |
| [alpine-react](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-react) | 1.2.1 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-react@1.2.1/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-react@1.2.1) |
| [alpine-react-ts](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-react-ts) | 1.2.1 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-react-ts@1.2.1/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-react-ts@1.2.1) |
| [alpine-redis](https://hub.docker.com/repository/docker/lupusmichaelis/alpine-redis) | 1.2.1 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/alpine-redis@1.2.1/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/alpine-redis@1.2.1) |
| [debian](https://hub.docker.com/repository/docker/lupusmichaelis/debian) | 1.2.2 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/debian@1.2.2/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/debian@1.2.2) |
| [igor](https://hub.docker.com/repository/docker/lupusmichaelis/igor) | 1.2.3 | [![pipeline status](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/badges/lupusmichaelis/igor@1.2.3/pipeline.svg)](https://gitlab.com/LupusMichaelis/lupus-dev-toolkit/-/commits/lupusmichaelis/igor@1.2.3) |
